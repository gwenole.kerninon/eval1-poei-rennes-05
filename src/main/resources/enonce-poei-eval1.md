# POEI Rennes 05 : Evaluation 1 (Java, POO)

## Livrables et critères d'évaluation

 * Le code source doit être présent sur un projet gitlab créé dans le groupe ZAcademy Promo 05 avec votre nom et prénom en description
 * La partie 2 implique de modifier le code de la partie 1. Pensez à créer un tag ou une branche pour que les deux parties soient clairement idéntifiées (demandez de l'aide si nécéssaire)
 * Pensez aux tests unitaires
 * Les branches autres que main ou celles spécifiées par vous comme contenant du code fini seront ignorées pour la notation. Pensez à utiliser des branches si vous démarrez du travail sans être certain de pouvoir le terminer (demandez de l'aide si nécéssaire).
 
 Pour l'évaluation, les critères généraux suivants seront utilisés : 
 
 * Le programme compile et s'exécute sans erreur (50%)
 * Les classes et méthodes développées correspondent au cahier des charges (30%)
 * Les classes et méthodes développées sont testées avec une méthode main et des tests unitaires (20%) 

## Partie 1 : Développer à partir de spécifications techniques

Cette partie teste les connaissances sur la syntaxe du langage Java (Création de classes, de variables, appels de méthodes...) et la capacité à faire intéragir plusieurs éléments d'un programme.

### Instructions

Le programme reprend la fonctionnalité de Marmiton sur la recherche de Recette en fonction des éléments présents dans son frigo.

Dans cette première partie, nous allons définir un frigo appartenant à une personne et gérer l'ajout ou la suppression d'aliment dans ce frigo.

Les opérations possibles sont les suivantes : 
 * Récupérer les informations du frigo
 * Ajouter un aliment dans son frigo
 * Retirer un aliment dans son frigo
 
#### Modélisation

Les instructions suivantes correspondent au diagramme de classes fourni en annexe.

 1. Coder la classe `Food` qui représente un aliment. Une fois créée, une instance de cette classe ne peut pas changer de nom.
    * Son nom est une chaine de caractère `String`
    * Le constructeur doit prendre en paramètre le nom de l'aliment
     * La méthode `getName` permet de récupérer le nom de l'aliment
 2. Coder la classe `Fridge` qui représente un frigo.
    * La frigo contient une liste d'aliment (`Food`)
    * Le constructeur doit initialiser la liste d'aliment à vide. 
    * La méthode `getNbFood` permet de récupérer le nombre d'aliment présent dans le frigo.
    * La méthode `existIn` permet de tester l'existance d'un aliment dans le frigo. Le nom sert de clé pour identifier un aliment.
    * La méthode `add` permet d'ajouter un aliment dans le frigo. Le nom sert de clé pour identifier un aliment. **La méthode ne fait rien si l'aliment est déjà indiqué comme présent**.
    * La méthode `remove` permet de supprimer un aliment dans le frigo. Le nom sert de clé pour identifier un aliment. **La méthode ne fait rien si l'aliment n'existe pas dans le frigo**.
    * La méthode `toString()` permet de récupérer une chaine de caractère avec le nombre d'aliment et le nom de chaque aliment présent dans le frigo.
 3. Coder la classe `Person` qui représente une personne.
    * La personne a un nom et un prénom (`lastname`, `firstname`) et un frigo (`fridge`)
    * Le constructeur doit prendre en paramètre le nom et le prénom et initialiser un frigo vide. 
    * Les méthodes `getLastname` et `getFirstname` permettent respectivement de récupérer le nom et le prénom.
    * La méthode `getFridge` permet de récupérer le Frigo de la personne
    * La méthode `addInMyFridge` permet d'ajouter un aliment dans le frigo. Le nom sert de clé pour identifier un aliment. **La méthode ne fait rien si l'aliment est déjà indiqué comme présent**.
    * La méthode `removeInMyFridge` permet de supprimer un aliment dans le frigo. Le nom sert de clé pour identifier un aliment. **La méthode ne fait rien si l'aliment n'existe pas dans le frigo**.
 
#### Code du programme

Les instructions suivantes correspondent au diagramme de séquence fourni en annexe.

écrire un programme qui effectue les actions suivantes : 

 1. Créer une personne avec pour nom "Pitt" et prénom "Brad"
 2. Récupérer le frigo de Brad Pitt et l'afficher dans la console (il faudra le convertir en String)
 3. Ajouter un aliment "Riz" dans son Frigo en appelant la méthode `addInMyFridge`.
 4. Afficher l'état du frigo (cf. étape 2)
 5. Ajouter un aliment "Potimarron" dans son Frigo en appelant la méthode `addInMyFridge`
 6. Afficher l'état du frigo (cf. étape 2)
 7. Ajouter de nouveau un aliment "Riz" dans son Frigo en appelant la méthode `addInMyFridge` (ceci ne devrait avoir aucun effet).
 8. Afficher l'état du frigo (cf. étape 2)
 7. Retirer un aliment "Riz" dans son Frigo en appelant la méthode `removeInMyFridge`.
 8. Afficher l'état du frigo (cf. étape 2)
 9. Retirer un aliment "Riz" dans son Frigo en appelant la méthode `removeInMyFridge` (ceci ne devrait avoir aucun effet).
 10. Afficher l'état du frigo (cf. étape 2)


## Partie 2 : Développer à partir d'un cahier des charges fonctionnel

Cette partie teste la capacité à modéliser des classes et à les utiliser à partir d'une description non technique. Dans cette partie, ils faudra
 1. Identifier les éléments du cahier des charges qui méritent la création d'une classe, et leurs relation avec les autres éléments (héritage, ...)
 2. Déterminer les informations qui caractérisent ces concepts pour en faire des attributs
 2. Déterminer les actions possibles sur ces concepts et en faire des méthodes ou spécialiser des méthodes (override)
 
Le code de cette partie prend comme point de départ le code de la partie 1. N'oubliez donc pas de créer un tag git sur le commit final de la partie 1 (demandez de l'aide si nécéssaire pour la création d'un tag git)

### Cahier des charges 

_Un exemple de scénario de test est présent à la fin du cahier des charges pour aider à la compréhension_

#### Nouveaxu types d'utilisateur et gestion d'un livre de recette

On va ajouter deux nouveaux types de personne et la gestion d'un livre de recette.

Le cuisinier, en plus de son nom et prénom, a également un attribut indiquant le nom du restaurant dans lequel il travaille.
Le chef, en plus de son nom et prénom et du nom du restaurant, a également un nombre d'étoile. 
Un chef et un cuisiner sont également identifés comme étant des auteurs.

Le livre de recette contient une liste de recette et permet d'enregistrer de nouvelles recettes.
Une recette possède un nom, un auteur et un tableau d'ingrédient. Un ingrédient est un aliment mais possède en plus la notion de quantité et d'unité de mesure. Nous allons avoir besoin de 3 unités de mesure : UNITE, GRAMME, VERRE. Une recette ne peut-être ajoutée sans ingrédient.

Le livre de recette n'est pas lié à un auteur, plusieurs auteurs peuvent y ajouter des recettes.
Seule une personne identifié comme étant un auteur peut enregistrer une nouvelle recette.

Le livre de recette permet d'enregistrer de nouvelles recettes en s'assurant qu'il n'y a pas déjà une recette avec le même nom. 
Le livre de recette permet également d'effecter une recherche à partir du frigo d'une personne. Le cuisinier et le chef possèdent également un frigo et peuvent donc utiliser cette fonctionnalité.

La fonctionnalité de recherche par frigo renvoie la première recette dont un aliment correspond avec un ingrédient (même nom).

Une recette peut-être affichée dans la console comme suit : <Nom recette> par <Prénom Nom Auteur> : <Nom Ingredient 1> <Quantité Ingrédient 1>  <Unité de messure Ingrédient 1>, <Nom Ingredient 2> <Quantité Ingrédient 2>  <Unité de messure Ingrédient 2>

Un auteur peut récupérer la liste des recettes qu'il a écrit. Pour identifier un auteur on utilisera son nom et son prénom.

Dans votre implémentation, il ne doit pas être possible d'appeler les fonctions d'ajout d'une recette ou de récupération de la liste des recettes d'un auteur à l'aide d'une personne qui n'est pas identifiée comme étant auteur. C'est-à-dire que votre code ne doit pas compilé si vous essayer d'appeler une de ces méthodes en lui passant une personne comme Brad Pitt si l'on reprend l'exemple de la partie 1.
    
#### Scénario d'exemple

Pour tester les classes développées dans cette partie, voilà un scénario d'exemple qui peut être développé dans la méthode main (ou dans un test unitaire...)

Ce scénario reprend la suite du scénario précédement implémenté.
 1. Créer un nouveau livre de recette vide
 2. Créer un chef "Cyril Lignac" du restaurant "Le Chardenoux" et 1 étoile
 3. Ajouter une recette "Potimarron farci au boeuf haché, riz et champignons" avec comme Auteur "Cyril Lignac" et avec pour ingrédients : 
  - "Riz" 1 VERRE
  - "Potimarron" 1 UNITE
  - "Boeuf" 200 GRAMME
  - "Champignons" 120 GRAMME
 4. Appeler la fonction de recherche d'une recette à partir du frigo de Brad Pitt -> Récupération de la recette "Potimarron farci au boeuf haché, riz et champignons"
 5. Appeler la fonction du livre de recette pour récupérer les recettes écrites par un auteur avec l'auteur "Cyril Lignac" - Récupération d'une liste contenant la recette "Potimarron farci au boeuf haché, riz et champignons"
 6. Appeler la fonction de recherche d'une recette à partir du frigo de Cyril Lignac -> null
 7. Ajout d'un Aliment Boeuf dans le frigo de Cyril Lignac
 8. Appeler la fonction de recherche d'une recette à partir du frigo de Cyril Lignac -> Récupération de la recette "Potimarron farci au boeuf haché, riz et champignons"
 9. Créer un cuisinier "Jean Imbert" du restaurant "Plaza Athénée"
10. Ajouter une recette "Fondant au chocolat" avec comme Auteur Jean Imbert et avec pour ingrédients : 
  - "Oeuf" 2 UNITE
  - "Sucre roux" 50 GRAMME
  - "Beurre salé" 100 GRAMME
  - "Farine" 25 GRAMME
  - "Chocolat" 200 GRAMME
11. Appeler la fonction du livre de recette pour récupérer les recettes écrites par un auteur avec l'auteur Jean Imbert - Récupération d'une liste contenant la recette "Fondant au chocolat"
12. Appeler la fonction de recherche d'une recette à partir du frigo de Jean Imbert -> Liste vide
13. Ajout d'un aliment Chocolat dans le frigo de Jean Imbert
14. Appeler la fonction de recherche d'une recette à partir du frigo de Jean Imbert -> Récupération de la recette "Fondant au chocolat"
 
## Bonus
 
Les objectifs suivants sont facultatifs. Ils seront valorisés s'ils sont présents mais n'entrainent aucune pénalité s'ils sont absent. 

#### Gestion des auteurs

Parmi les auteurs, il est possible d'avoir des homonymes. Lors de la création d'un auteur, on va donc générer un identifiant aléatoire pour l'utiliser ensuite comme identifiant d'auteur.
 
Une fois l'auteur créé, son numéro d'auteur ne peut plus changer mais son nom peut être mis à jour. 
 
Modifier la fonction de recherche de recette par auteur pour qu'elle utilise cet identifiant plutôt que le nom et prénom.

#### Ajout/Retrait impossible

Pour les opérations `addInMyFridge`, `removeInMyFridge` au lieu de ne rien faire, renvoyer une exception spécifique. 

Même chose pour l'ajout d'une recette déjà existante.

#### Stream et Optional

Implémenter la méthode de recherche par frigo à l'aide d'un stream pour parcourir la liste des recettes.
Modifier le retour de la méthode pour renvoyer un Optional.
Modifier le scénario de la méthode Main ou/et le test unitaire associé pour tester la récupération ou non d'une recette.

#### Frigo/Recette immutable

Il est possible que votre code présente une faille par les méthodes renvoyant des objets Recette et Frigo.

En effet, ce sont des objets donc ce sont des référénces que l'on récupère. Une modification de l'objet Recette ou Frigo dans la méthode main va donc avoir des effets de bord. La modification ne restera pas locale à Main mais sera bien propagée au niveau de la Personne ou du Livre des recettes.

Résoudre le problème en rendant les classes Recette et Frigo immutable.

