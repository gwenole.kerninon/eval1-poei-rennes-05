import recipe.book.RecipeBook;
import recipe.food.Food;
import recipe.food.Ingredient;
import recipe.food.MesureType;
import recipe.person.Chef;
import recipe.person.Cooker;
import recipe.person.Fridge;
import recipe.person.Person;

public class Main {
    public static void main(String[] args) {

        Person bradPitt = new Person("Pitt", "Brad");
        displayFridgeState(bradPitt);

        bradPitt.addInMyFridge(new Food("Riz"));
        bradPitt.addInMyFridge(new Food("Potimarron"));
        displayFridgeState(bradPitt);

        bradPitt.addInMyFridge(new Food("Riz"));
        displayFridgeState(bradPitt);

        bradPitt.removeInMyFridge(new Food("Riz"));
        displayFridgeState(bradPitt);

        bradPitt.removeInMyFridge(new Food("Riz"));
        displayFridgeState(bradPitt);

        RecipeBook recipeBook = new RecipeBook();

        Chef cyrilLignac = new Chef("Lignac", "Cyril", "Le Chardenoux", 1);
        recipeBook.addRecipe("Potimarron farci au boeuf haché, riz et champignons", cyrilLignac,
                new Ingredient[]{
                        new Ingredient("Riz", 1, MesureType.VERRE),
                        new Ingredient("Potimarron", 1, MesureType.UNITE),
                        new Ingredient("Boeuf", 200, MesureType.GRAMME),
                        new Ingredient("Champignons", 120, MesureType.GRAMME)
                });

        System.out.println(recipeBook.searchBy(bradPitt.getFridge()));
        System.out.println(recipeBook.searchBy(cyrilLignac.getFridge()));

        cyrilLignac.addInMyFridge(new Food("Boeuf"));
        System.out.println(recipeBook.searchBy(cyrilLignac.getFridge()));

        Cooker jeanImbert = new Cooker("Imbert", "Jean", "Plaza Athénée");
        recipeBook.addRecipe("Fondant au chocolat", jeanImbert,
                new Ingredient[]{
                        new Ingredient("Oeuf", 2, MesureType.UNITE),
                        new Ingredient("Sucre roux", 50, MesureType.GRAMME),
                        new Ingredient("Beurre salé", 100, MesureType.GRAMME),
                        new Ingredient("Farine", 25, MesureType.GRAMME),
                        new Ingredient("Chocolat", 200, MesureType.GRAMME),
                });

        jeanImbert.addInMyFridge(new Food("Chocolat"));
        System.out.println(recipeBook.searchBy(jeanImbert.getFridge()));

    }

    public static void displayFridgeState(Person person) {
        Fridge fridge = person.getFridge();
        System.out.println(fridge.toString());
    }
}
