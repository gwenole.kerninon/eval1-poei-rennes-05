package recipe.book;

import recipe.food.Food;
import recipe.food.Ingredient;
import recipe.person.Author;

import java.util.List;
import java.util.stream.Collectors;

public class Recipe {

    private final List<Ingredient> ingredients;
    private final String name;
    private final Author author;

    public Recipe(String name, Author author, List<Ingredient> ingredients) {
        this.name = name;
        this.author = author;
        this.ingredients = ingredients;
    }

    public List<Ingredient> getIngredients() {
        return ingredients;
    }

    public String getName() {
        return name;
    }

    public Author getAuthor() {
        return author;
    }

    @Override
    public String toString() {
        String result = this.name + " par " + this.author.getFirstname() + " " + this.getAuthor().getLastname() + " : ";
        result += this.ingredients.stream().map(Ingredient::toString).collect(Collectors.joining(", "));
        return result;
    }
}
