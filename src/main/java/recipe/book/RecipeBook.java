package recipe.book;

import recipe.food.Ingredient;
import recipe.person.Author;
import recipe.person.Fridge;

import java.util.*;
import java.util.stream.Collectors;

public class RecipeBook {

    public final List<Recipe> recipes;

    public RecipeBook() {
        this.recipes = new ArrayList<>();
    }

    public void addRecipe(String name, Author author, Ingredient[] ingredients) {
        if(this.recipes.stream().noneMatch(r -> r.getName().equalsIgnoreCase(name))) {
            this.recipes.add(new Recipe(name, author, Arrays.stream(ingredients).toList()));
        }
    }

    public Recipe searchBy(Fridge fridge) {
        return this.recipes.stream().filter(
                (Recipe r) -> r.getIngredients().stream().anyMatch(fridge::existsIn)
        ).findFirst().orElse(null);
    }

    public List<Recipe> writedBy(Author author) {
        return this.recipes.stream().filter(
                (Recipe r) -> r.getAuthor().getFirstname().equals(author.getFirstname())
                        && r.getAuthor().getLastname().equals(author.getLastname())
        ).collect(Collectors.toList());
    }

}
