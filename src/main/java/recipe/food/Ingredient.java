package recipe.food;

public class Ingredient extends Food {

    private final MesureType mesureType;
    private final int quantity;

    public Ingredient(String name, int quantity, MesureType mesureType) {
        super(name);
        this.quantity = quantity;
        this.mesureType = mesureType;
    }

    @Override
    public String toString() {
        return this.name + " " + this.quantity + " " + this.mesureType.toString();
    }
}
