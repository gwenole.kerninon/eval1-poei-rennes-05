package recipe.food;

public enum MesureType {
    UNITE,
    VERRE,
    GRAMME
}
