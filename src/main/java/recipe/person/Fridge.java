package recipe.person;

import recipe.food.Food;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Fridge {

    private final List<Food> foods = new ArrayList<>();

    public Fridge() {
    }

    public int getNbFoods() {
        return this.foods.size();
    }

    public boolean existsIn(Food food) {
        return this.foods.stream().anyMatch(f -> f.getName().equalsIgnoreCase(food.getName()));
    }

    public void add(Food food) {
        if (!this.existsIn(food)) {
            this.foods.add(food);
        }
    }

    public void remove(Food food) {
        this.foods.removeIf(f -> f.getName().equalsIgnoreCase(food.getName()));
    }

    public String toString() {
        return this.foods.size() + " Aliment(s) dans le frigo : " +
                this.foods.stream().map(Food::getName).collect(Collectors.joining(", "));
    }

}
