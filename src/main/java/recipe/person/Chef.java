package recipe.person;

public class Chef extends Cooker implements Author {

    private int nbStars;

    public Chef(String lastname, String firstname, String restaurant, int nbStars) {
        super(lastname, firstname, restaurant);
        this.nbStars = nbStars;
    }
}
