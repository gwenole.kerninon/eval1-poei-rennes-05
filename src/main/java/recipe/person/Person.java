package recipe.person;

import recipe.food.Food;

public class Person {

    private final String lastname;
    private final String firstname;
    private final Fridge fridge;

    public Person(String lastname, String firstname) {
        this.lastname = lastname;
        this.firstname = firstname;
        this.fridge = new Fridge();
    }

    public void addInMyFridge(Food food) {
        this.fridge.add(food);
    }

    public void removeInMyFridge(Food food) {
        this.fridge.remove(food);
    }

    public String getLastname() {
        return lastname;
    }

    public String getFirstname() {
        return firstname;
    }

    public Fridge getFridge() {
        return fridge;
    }
}
