package recipe.person;

public class Cooker extends Person implements Author {

    protected String restaurant;

    public Cooker(String lastname, String firstname, String restaurant) {
        super(lastname, firstname);
        this.restaurant = restaurant;
    }
}
