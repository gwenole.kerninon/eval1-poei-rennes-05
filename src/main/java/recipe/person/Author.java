package recipe.person;

public interface Author {

    String getFirstname();
    String getLastname();

}
